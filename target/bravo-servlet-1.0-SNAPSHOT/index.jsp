<%--
  Created by IntelliJ IDEA.
  User: student10
  Date: 7/9/20
  Time: 3:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@include file="WEB-INF/partials/navbar.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Home</title>
  </head>
  <body>
  <h1>  Welcome to my Servlet Project's Home Page!</h1>

  <p>Here we see the different tools we have within Java using JSP / JSTL / servlets</p>

  <p>Click on a link in the NavBar to see different exercises and examples - then refer to the code.</p>

  <%@include file="WEB-INF/partials/footer.jsp"%>
  </body>
</html>
