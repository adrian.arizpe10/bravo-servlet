<%--
  Created by IntelliJ IDEA.
  User: student10
  Date: 7/10/20
  Time: 1:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="WEB-INF/partials/navbar.jsp"%>
<html>
<head>
    <title>JSP-Lesson</title>
</head>
<body>
    <h3>JSP Expression</h3>
    <p>Converting a string to uppercase: <%=new String("I'm not yelling!").toUpperCase()%></p>
    <br>
<p>25 multiplied by 4: <%=25 * 4%> </p>
    <br>
<p>Is 75 less than 68? (True or false): <%=75 < 68%></p>

<h3>JSP Scriplet</h3>
<%
    for (int i = 1; i <= 5; i++){
        out.println(i);
    }
%>

<h3>JSP Declaration</h3>
<%!
    String makeItLower(String data){
        return data.toLowerCase();
    }
%>
<%--call our makeItLower() method--%>
<%=
    makeItLower("Hello BravO!")
%>

<%--Expression Language (EL)--%>
<h3>Expression Language (EL)</h3>
    <%@include file="WEB-INF/partials/el.jsp"%>
<%--    Alternative Syntax for include directives--%>
<%--    <jsp:include page="WEB-INF/partials/el.jsp"></jsp:include>--%>
<%--<jsp:include page="WEB-INF/partials/el.jsp">--%>

<p>JSP EL</p>
<p>The JSP Expression Language, is more syntax that we can use in combining our JSP files</p>
<p>EL makes it easy to access attributes from the Request Object</p>
<p>- making it a convenient way of accessing properties on objects</p>

    <%@include file="WEB-INF/partials/footer.jsp"%>
</body>
</html>
