<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student10
  Date: 7/10/20
  Time: 2:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="WEB-INF/partials/navbar.jsp"%>

<html>
<head>
    <title>JSTL</title>
</head>
<body>

    <h1>JSTL Directives</h1>
    <h3>otherwise, choose, when</h3>

<%--    <c:choose>--%>
<%--        <c:when test="${boolean_expression_1}">--%>
<%--            <p>display if expression 1 is true</p>--%>
<%--        </c:when>--%>
<%--        <c:when test="${boolean_expression_2}">--%>
<%--            <p>display if expression 2 is true</p>--%>
<%--        </c:when>--%>
<%--        <c:otherwise>--%>
<%--            <p>none of the above tests were true</p>--%>
<%--        </c:otherwise>--%>
<%--    </c:choose>--%>

<%--Choose example--%>
<c:set value="11" var="num">

</c:set>

<c:choose>
    <c:when test="${num % 2 == 0}">
        <c:out value="${num} is an even number">

        </c:out>
        <p>Number is even</p>
    </c:when>
    <c:otherwise>
        <c:out value="${num} is an odd number">

        </c:out>
    </c:otherwise>
</c:choose>

<h3>c:forEach</h3>
<%--<c:forEach items="${collection}" var="element">--%>
<%--    --%>
<%--</c:forEach>--%>

<%--<c:forEach var="i" begin="1" end="5">--%>
<%--    Item <c:out value="${i}"></c:out>--%>
<%--</c:forEach>--%>

<%
    request.setAttribute("numbers", new int[] {1, 2, 3, 4, 5});
%>
<ul>
    <c:forEach items="${numbers}" var="num">
        <li>${num}</li>
    </c:forEach>
</ul>

<%--<h3>if tag</h3>--%>
<%--<p>- allows us to conditionally show a piece of HTML</p>--%>
<%--<c:if test="${boolean-expression}">--%>
<%--    <p>boolean is true</p>--%>
<%--</c:if>--%>

<%--<c:if test="${isAdmin}">--%>
<%--    <h1>Welcome back admin!</h1>--%>
<%--&lt;%&ndash;    extra content, only viewable by admin&ndash;%&gt;--%>
<%--</c:if>--%>

    <%@include file="WEB-INF/partials/footer.jsp"%>
</body>
</html>
