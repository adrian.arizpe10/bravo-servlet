<%--
  Created by IntelliJ IDEA.
  User: student10
  Date: 7/14/20
  Time: 1:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Session and Cookie Login</title>
<%--    Bootstrap--%>
<%--    <jsp:include page="/WEB-INF/partials/bootstrap.jsp"/>--%>
</head>
<body>

<h1>Please Log In</h1>

<div class="container">
    <form action="/admin-login" method="post">
        <div class="form-group">
            <input type="text" id="username" name="username" placeholder="Username">
        </div>
        <div class="form-group">
            <input type="password" id="password" name="password" placeholder="Password">
        </div>
        <input type="submit" value="Log In">
    </form>
</div>


</body>
</html>
