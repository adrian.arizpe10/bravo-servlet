<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student10
  Date: 7/13/20
  Time: 3:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>WishList</title>
</head>
<body>

<h3>Here is the wish list</h3>
<table border="1">
    <tr>
        <th>name</th>
        <th>category</th>
        <th>price</th>
    </tr>
    <%--    JSTL--%>

    <c:forEach items="${listOfWishes}" var="wishList">
        <tr>
            <td>${wishList.name}</td>
            <td>${wishList.category}</td>
            <td>$${wishList.price}</td>
        </tr>
    </c:forEach>
</table>

<a href="/wishlist/add-item">Add to my Wish List</a>

</body>
</html>
