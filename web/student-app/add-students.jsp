<%--
  Created by IntelliJ IDEA.
  User: student10
  Date: 7/13/20
  Time: 2:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Student</title>
</head>
<body>

<form action="/students/add-student" method="post">
    <label for="firstName">First Name</label>
    <input type="text" name="firstName" id="firstName">
    <br>
    <label for="lastName">Last Name</label>
    <input type="text" name="lastName" id="lastName">
    <br>
    <label for="email">Email</label>
    <input type="email" name="email" id="email">
    <br>
    <input type="submit">
</form>
</body>
</html>
