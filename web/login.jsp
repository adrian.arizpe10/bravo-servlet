<%--
  Created by IntelliJ IDEA.
  User: student10
  Date: 7/13/20
  Time: 8:39 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="WEB-INF/partials/navbar.jsp"%>


<html>
<head>
    <title>Login</title>
</head>
<body>

<h1>Please log in below</h1>

<div>
<form action="/login" method="post">
    <input type="text" id="username" name="username" placeholder="Username...">
    <br>
    <input type="password" id="password" name="password" placeholder="Password...">
    <br>
    <input type="submit" name="login">
</form>
</div>


<c:choose>
    <c:when test="${param.username == 'admin' && param.password == 'password'}">
        <%
            response.sendRedirect("/profile.jsp");
        %>
    </c:when>
</c:choose>


<%@include file="WEB-INF/partials/footer.jsp"%>
</body>
</html>
