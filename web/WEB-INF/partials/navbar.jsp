<%--
  Created by IntelliJ IDEA.
  User: student10
  Date: 7/10/20
  Time: 1:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Navbar Partial</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body {
            margin: 0;
            font-family: Arial, Helvetica, sans-serif;
        }
        .topnav {
            overflow: hidden;
            background-color: #333;
        }
        .topnav a {
            float: left;
            color: #f2f2f2;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }
        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }
        .topnav a.active {
            background-color: #4CAF50;
            color: white;
        }
    </style>
</head>
<body>
<div class="topnav">
    <a class="active" href="/">Home</a>
    <a href="/login">Login</a>
    <a href="/jstl-lesson.jsp">JSTL</a>
    <a href="/jsp-lesson.jsp">JSP</a>
    <a href="/logout">Log Out</a>
</div>
</body>
</html>
