package StudentApp;
// class that provides access to our DAO
public class DaoFactory {
    //Field
    private static Students studentsDao;

    public static Students getStudentsDao(){
        if (studentsDao == null){
            studentsDao = new ListStudents();
        }
        return studentsDao;
    }
}
