import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "loginServlet", urlPatterns = "/logins.jsp")
public class loginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String user = request.getParameter("username");
        String pass = request.getParameter("password");

        if (user.equals("admin") && pass.equals("password")){
            response.sendRedirect("/profile.jsp");
        } else if (user.equals("stevejobs") && pass.equals("apple")){
            response.sendRedirect("http://apple.com");
        } else if (user.equals("billgates") && pass.equals("mircosoft")) {
            response.sendRedirect("https://www.microsoft.com/en-us");
        } else {
            response.sendRedirect("/logins.jsp");
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
