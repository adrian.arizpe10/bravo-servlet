package sessionCookies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "AdminProfileServlet", urlPatterns = "/admin-profile")
public class AdminProfileServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        boolean isUser = false;

        if (session.getAttribute("user") != null){
            isUser = (boolean) session.getAttribute("user");
            //session.getAttribute - used when we want to retrieve information from the session
        }
        //redirects the user, if he/she hasn't logged in
        if (isUser){
            request.setAttribute("username", session.getAttribute("username"));
            request.getRequestDispatcher("/cookieSessions/admin-profile.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("/cookieSessions/admin-login.jsp").forward(request, response);
        }
    }
}
