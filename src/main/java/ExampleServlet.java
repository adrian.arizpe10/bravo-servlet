import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ExampleServlet", urlPatterns = "/example-servlet")
public class ExampleServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    //1. Get the PrintWriter

        PrintWriter printWriter = response.getWriter();

        //2. Generate HTML content
        printWriter.println("<h1>Example Servlet</h1>");
        printWriter.println("<p>Created by Bravo</p>");
        printWriter.println("Time on the server is: " + new java.util.Date());

        printWriter.println("<a href=\"/count\">Go to count</a>");
    }
}
