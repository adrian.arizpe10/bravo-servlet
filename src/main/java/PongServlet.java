import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "PongServlet", urlPatterns = "/pong")
public class PongServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //set content type
        response.setContentType("text/html");


        //Get the PrintWriter
        PrintWriter writer = response.getWriter();

        //generate the content
        writer.println("<h3>Hello From PongServlet</h3>");
        writer.println("<a href=\"/ping\">Go to ping</a>");

    }
}
