import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "PingServlet", urlPatterns = "/ping")
public class PingServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //set content type
        response.setContentType("text/html");

        //Get the PrintWriter
        PrintWriter writer = response.getWriter();
        writer.println("<h3>Hello From PingServlet</h3>");
        writer.println("<a href=\"/pong\">Go to pong</a>");


        HttpSession session = request.getSession();
        boolean isUser = false;
        if (session.getAttribute("user") != null){
            isUser = (boolean) session.getAttribute("user");
        }
        if (isUser){
            request.setAttribute("username", session.getAttribute("username"));
            request.getRequestDispatcher("/ping").forward(request, response);
        } else {
            request.getRequestDispatcher("/login").forward(request, response);
        }

    }
}
