import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "HelloWorldServlet", urlPatterns = "/hello-world")
public class HelloWorldServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //set to get a parameter
        String name = request.getParameter("name");

        PrintWriter writer = response.getWriter();

        if (name == null){
            writer.println("<h1>Hello World!</h1>");
        }
        else if (name.equals("stevejobs")){
            response.sendRedirect("https://www.apple.com");
        }else {
            writer.println("<h1>Hello, " + name + "!</h1>");
        }

    }
}
