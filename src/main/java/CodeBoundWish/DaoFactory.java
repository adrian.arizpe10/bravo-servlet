package CodeBoundWish;
//class provides access to our DAO
public class DaoFactory {
    private static WishList listDao;

    public static WishList getListDao(){
        if (listDao == null){
            listDao = new WishList();
        }
        return listDao;
    }
}
