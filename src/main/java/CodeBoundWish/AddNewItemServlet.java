package CodeBoundWish;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddNewItemServlet", urlPatterns = "/wishlist/add-item")
public class AddNewItemServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WishList listDao = DaoFactory.getListDao();

        String newName = request.getParameter("name");
        String newCategory = request.getParameter("category");
        double newPrice = Double.parseDouble(request.getParameter("price"));

        Wish wish = new Wish(newName, newCategory, newPrice);

        listDao.insert(wish);

        response.sendRedirect("/wishlist");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
request.getRequestDispatcher("/WishList/add-wishes.jsp").forward(request, response);
    }
}
