package CodeBoundWish;

import CodeBoundWish.DaoFactory;
import CodeBoundWish.Wish;
import CodeBoundWish.WishList;
import CodeBoundWish.Wishes;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "CodeBoundWish.ShowWishListServlet", urlPatterns = "/wishlist")
public class ShowWishListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        WishList listDao = DaoFactory.getListDao();

        List<Wish> wishes = listDao.all();

        request.setAttribute("listOfWishes", wishes);
        request.getRequestDispatcher("/WishList/wishList.jsp").forward(request, response);

    }
}
