package CodeBoundWish;

import java.util.List;

public interface Wishes {
    List<Wish> all();
    void insert(Wish wish);
}
