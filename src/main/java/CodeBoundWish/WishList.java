package CodeBoundWish;

import java.util.ArrayList;
import java.util.List;

//DAO Implementation
public class WishList implements Wishes {
    // field
    private List<Wish> wishes = new ArrayList<>();

    public WishList(){
        insert(new Wish("Dewalt Impact Driver", "Tool", 200));
        insert(new Wish("Kayak", "Recreational", 500));
        insert(new Wish("Table Saw", "Tool", 400));
    }

    @Override
    public void insert(Wish wish){
        this.wishes.add(wish);
    }
    @Override
    public List<Wish> all(){
        return this.wishes;
    }
}
